﻿using System;

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.IO;
using Microsoft.Boogie;
using Microsoft.Boogie.GraphUtil;
using System.Diagnostics.Contracts;
using Microsoft.Basetypes;
using Microsoft.Boogie.VCExprAST;
//using Microsoft.Boogie.SMTLib;
using VC;

namespace VCGeneration {
    using Bpl = Microsoft.Boogie;
    class InferInvariant {
        private HashSet<VCExpr> pathConditions; // path conditions of all the paths in foo
        public void setPathConditions ( HashSet<VCExpr> pc ) {
            this.pathConditions = pc;
        }

        //public VCExpr getPost ( VCExpr inState ) {
        //    return null;
        //
        /**
         * Returns the state that holds after executing the path whose path condition is pc 
         * when the input state is inState.
         */
        private VCExpr getPost ( VCExpr inState,VCExpr pc ) {
            //check if inState and pc is  SAT
            return null;
        }
    }
    class PathConditionsVC {
        private static Program prog;
        private static bool PrintPath = false;
        private static bool printPC = true;
        public static ProverInterface prover;

  

        internal static void printPathConditions ( HashSet<VCExpr> PCs ) {
            //PCHandler.printPathConditions(PCs);
            foreach (VCExpr expr in PCs) {
                System.Console.Write(expr.ToString());
                System.Console.WriteLine("******\n");
                //Microsoft.Boogie.SMTLib.SMTLibLineariser.
                //Contract.Requires(e != null);
                //Contract.Requires(namer != null);
                //Contract.Ensures(Contract.Result<string>() != null);

                //StringWriter sw = new StringWriter();
                //SMTLibExprLineariser lin = new SMTLibExprLineariser(sw,namer,opts);
                //Contract.Assert(lin != null);
                //lin.Linearise(e,LineariserOptions.Default);
                //Console.WriteLine( cce.NonNull(sw.ToString()));
            }

        }

        /// <summary>
        /// This method generates the path conditions with the invariants adapted for the new version of the global vairable after each methdo call.
        /// 
        /// known issues :
        /// 1. if g0 is the latest version of g before method call. after the method call inv[g/g1] is placed. But after that if 
        /// there is write to g. then it is to g2. but this should not cause problems.
        /// </summary>
        /// <param name="impl"></param>
        /// <param name="source"></param>
        /// <param name="invariant"></param>
        /// <returns></returns>
        internal static HashSet<VCExpr> getPathConditions ( Implementation impl,Block source,VCExpr invariant ) {
            Graph<Block> g = Program.GraphFromImpl(impl);
            HashSet<VCExpr> ret = new HashSet<VCExpr>();
            g.ComputeLoops();
            // later
            //if (g.getBackEdgeNodes().Count() > 0) {
            //    Console.WriteLine("Loops not handeled yet");
            //    Environment.Exit(0);
            //}
            HashSet<List<Block>> paths = getPaths(g,source);
            paths = unbraidPaths(paths);
            foreach (List<Block> path in paths) {
                VCExpr pc = getPathCondition(path,impl,invariant);
                //Console.WriteLine("\npath Condition : " + pc + '\n');
                ret.Add(pc);
                //Console.WriteLine(prover.GetType());
                //prover.Assert(pc,true);
                //prover.Check();              
                //ProverInterface.ErrorHandler handler = new ProverInterface.ErrorHandler();
                //ProverInterface.Outcome o = prover.CheckOutcome(handler);
                //Console.WriteLine("outcome : " + o);
            }
            //prover.Reset(null);           
            return ret;
        }
        static Function myFunction = null;
        private static VCExpr getPathCondition ( List<Block> path,Implementation impl,VCExpr invariant ) {
            var outputVariables = new List<Variable>();
            HashSet<Variable> modified = new HashSet<Variable>();
            VCExpr ret = invariant;
            VCExpressionGenerator gen = prover.VCExprGen;
            var exprGen = prover.Context.ExprGen;
            var translator = prover.Context.BoogieExprTranslator;
            //foreach (var b in path) {
            //    foreach (var cmd in b.cmds) {
            //        Console.Write(cmd);
            //    }
            //}

            for (int bCtr = 0,blockCount = path.Count; bCtr < blockCount; bCtr++) {
                Block curBlock = path.ElementAt(bCtr);
                Boolean skip = false;
                for (int sCtr = 0,stmtCount = curBlock.cmds.Count; sCtr < stmtCount; sCtr++) {
                    if (skip) {
                        skip = false;
                        continue;
                    }
                    VCExpr expr;
                    var cmd = curBlock.cmds.ElementAt(sCtr);
                    var acmd = cmd as AssumeCmd;

                    if (acmd == null) {
                        if (cmd is AssertCmd) {
                            expr = translator.Translate((cmd as AssertCmd).Expr);
                            expr = gen.Not(expr);
                        } else
                            continue;
                    } else {
                        expr = translator.Translate(acmd.Expr);
                        NAryExpr naryExpr = acmd.Expr as NAryExpr;
                        if (naryExpr != null && naryExpr.Fun is FunctionCall) {
                            FunctionCall call = naryExpr.Fun as FunctionCall;
                            Function function = call.Func;
                            List<Variable> newInparams = new List<Variable>();
                            List<Expr> newArgs = new List<Expr>(naryExpr.Args);
                            //Console.WriteLine("nary expression args : ");
                            //foreach (var a in newArgs)
                            //    Console.Write(a + ", ");
                            newInparams.AddRange(function.InParams);

                            for (int i = 0; i < impl.Proc.Modifies.Count(); i++) {
                                newInparams.RemoveRange(0,1);
                                newInparams.RemoveRange(newInparams.Count() - 1,1);
                                newArgs.RemoveRange(0,1);
                                newArgs.RemoveRange(newArgs.Count() - 1,1);
                            }
                            Variable retVar = newInparams.ElementAt(newInparams.Count() - 1);
                            Expr retVarExpr = newArgs.ElementAt(newArgs.Count() - 1);
                            VCExpr retVarVCExpr = translator.Translate(retVarExpr);
                            newInparams.RemoveRange(newInparams.Count() - 1,1);
                            newArgs.RemoveRange(newArgs.Count() - 1,1);                           
                            FunctionCall myCall = new FunctionCall(myFunction);
                            NAryExpr myExpr = new NAryExpr(Token.NoToken,myCall,newArgs);
                            myExpr.Resolve(new ResolutionContext(null));
                            myExpr.Typecheck(new TypecheckingContext(null));
                            prover.Context.DeclareFunction(myFunction,"");
                            //Console.WriteLine("my expr type : " + myExpr.Type);
                            expr = translator.Translate(myExpr);
                            expr = gen.Eq(expr,retVarVCExpr);

                            // get the substituted invarinat
                            VCExprSubstitution subst = new VCExprSubstitution();
                            SubstitutingVCExprVisitor substituter = new SubstitutingVCExprVisitor(gen);
                            List<Expr> l = new List<Expr>();
                            l.AddRange(impl.Proc.Modifies);
                            int gCount = impl.Proc.Modifies.Count();
                            List<Expr> incarExpr = new List<Expr>();
                            incarExpr.AddRange(naryExpr.Args);
                            incarExpr.RemoveRange(0,incarExpr.Count() - gCount);
                            List<VCExpr> orig = translator.Translate(l);
                            for (int ctr = 0; ctr < impl.Proc.Modifies.Count(); ctr++) {
                                if (orig.ElementAt(ctr) is VCExprVar) {
                                    VCExprVar v = orig.ElementAt(ctr) as VCExprVar;
                                    VCExpr k = translator.Translate(incarExpr.ElementAt(ctr));
                                    subst[v] = k;
                                    //Console.WriteLine(v + " is replaced with " + k);
                                } else {
                                    Console.WriteLine("error : 3");
                                    Environment.Exit(0);
                                }
                            }
                            ret = gen.AndSimp(ret,expr);
                            expr = substituter.Mutate(invariant,subst);
                            skip = true;        // skip the next statement                           
                        }
                    }
                    ret = gen.AndSimp(ret,expr);
                }
            }
            return ret;
        }

        /*
         * variable path has the invariant encoded into it already 
        */
        private static VCExpr post ( VCExpr path,Implementation impl ) {
            VCExprSubstitution subst = new VCExprSubstitution();
            VCExpressionGenerator gen = prover.VCExprGen;
            SubstitutingVCExprVisitor substituter = new SubstitutingVCExprVisitor(gen);
            var translator = prover.Context.BoogieExprTranslator;

            // substitute variables named as global varables
            for (int ctr = 0; ctr < impl.Proc.Modifies.Count; ctr++) {
                VCExprVar v = translator.Translate(impl.Proc.Modifies.ElementAt(ctr)) as VCExprVar;
                VCExprVar nuVar = gen.Variable(v.Name + "_my",v.Type);
                subst[v] = nuVar;
            }
            // conjunt foo_g = g for all global variables g
            VCExpr substitutedPath = substituter.Mutate(path,subst);
            for (int ctr = 0; ctr < impl.Proc.Modifies.Count; ctr++) {
                VCExprVar g = translator.Translate(impl.Proc.Modifies.ElementAt(ctr)) as VCExprVar;
                VCExprVar foo_g = gen.Variable(impl.Proc.Name + "_" + g.Name,g.Type);
                VCExpr eq = gen.Eq(g,foo_g);
                substitutedPath = gen.Add(substitutedPath,eq);
            }
            return substitutedPath;
        }
        private static bool isInvariant ( List<Block> path,VCExpr inv,Implementation impl ) {
            VCExpr pathConditionWithInvariant = getPathCondition(path,impl,inv);
            VCExpressionGenerator gen = prover.VCExprGen;
            gen.Implies(pathConditionWithInvariant,inv);

            // to continue form here
            // now check if inv implies pathConditionWithInvariant

            return false;
        }

        private static HashSet<List<Block>> unbraidPaths ( HashSet<List<Block>> paths ) {
            HashSet<List<Block>> newPaths = new HashSet<List<Block>>();
            foreach (var path in paths) {
                List<Block> newPath = new List<Block>();
                for (int ctr = 0,len = path.Count; ctr < len; ctr++) {
                    Block b = path.ElementAt(ctr);
                    Block newBlock = new Block();
                    newBlock.cmds = b.cmds;
                    newBlock.tok = b.tok;
                    newBlock.Label = b.Label;

                    newPath.Add(newBlock);
                }
                newPaths.Add(newPath);
            }
            foreach (var path in newPaths) {
                for (int ctr = 0; ctr < path.Count - 1; ctr++) {
                    Block cur = path.ElementAt(ctr);
                    Block nextBlock = path.ElementAt(ctr + 1);
                    List<Block> nextBlocks = new List<Block>();
                    nextBlocks.Add(nextBlock);
                    cur.TransferCmd = new GotoCmd(Token.NoToken,nextBlocks);
                }
            }
            return newPaths;
        }

        private static void printPath ( List<Block> list ) {
            if (!PrintPath) return;
            for (int b = 0,bSize = list.Count(); b < bSize; b++) {
                Block cur = list.ElementAt(b);
                Console.WriteLine("Block :" + cur.Label + " next : " + cur.TransferCmd);
                foreach (Cmd c in cur.cmds) {
                    Console.Write(c);
                }
            }
        }

        private static HashSet<List<Block>> getPaths ( Graph<Block> g,Block source ) {
            HashSet<List<Block>> ans = new HashSet<List<Block>>();
            List<Block> init = new List<Block>();
            init.Add(source);
            ans.Add(init);
            Boolean done = false;

            while (!done) {
                HashSet<List<Block>> toRemove = new HashSet<List<Block>>();
                HashSet<List<Block>> toAdd = new HashSet<List<Block>>();
                foreach (var path in ans) {
                    ReturnCmd ret = path.Last().TransferCmd as ReturnCmd;
                    if (ret == null) {
                        var nextNodes = g.Successors(path.Last());
                        //Console.WriteLine("for node :" + path.Last() + " next nodes are :" + path.Last().TransferCmd);
                        //Console.WriteLine("next node count : " + nextNodes.Count());
                        switch (nextNodes.Count()) {
                            case 0:
                                throw (new Exception("node with 0 successors"));
                                break;
                            case 1:
                                toRemove.Add(path);
                                List<Block> newPath = new List<Block>(path);
                                newPath.Insert(path.Count,nextNodes.ElementAt(0));
                                toAdd.Add(newPath);
                                //path.Insert(path.Count,nextNodes.ElementAt(0));
                                break;
                            default:
                                var prePath = path;
                                //ans.Remove(prePath);
                                toRemove.Add(path);
                                foreach (var node in nextNodes) {
                                    List<Block> nuPath = new List<Block>(prePath);
                                    nuPath.Insert(nuPath.Count,node);
                                    toAdd.Add(nuPath);
                                }
                                break;
                        }
                    }
                }
                foreach (var path in toRemove)
                    ans.Remove(path);
                foreach (var path in toAdd)
                    ans.Add(path);
                done = true;
                foreach (var path in ans) {
                    ReturnCmd ret = path.Last().TransferCmd as ReturnCmd;
                    if (ret == null) done = false;
                }
            }
            return ans;
        }

        internal static bool doAnalysis ( Implementation impl,Block source,Program program ) {
            VCExpressionGenerator gen = new VCExpressionGenerator();
            prog = program;
            var a = program.ExtractLoops();
            //Console.WriteLine("source block is : " + source);
            prover = ProverInterface.CreateProver(program,"errorLog",false,CommandLineOptions.Clo.ProverKillTime);            
            //Console.WriteLine("prover is  :" + prover.GetType());
            setupUninterpretedFunction(impl,program);
            prover.Context.DeclareFunction(myFunction,"");
            //Console.WriteLine("Function : " + myFunction);
            VCExpr invariant = getInvariant(impl);
            //Console.WriteLine("invarinat : " + invariant);
            //Environment.Exit(0);
            HashSet<VCExpr> pathConditions = getPathConditions(impl,source,invariant);
            CommandLineOptions.Clo.SIBoolControlVC = true;
            CommandLineOptions.Clo.UseLabels = false;
            //printPathConditions(pathConditions);
            VCExpr f = forAllOPCheck(impl, pathConditions, invariant);
            VCExpr f1 = forSomeOPCheck(impl, pathConditions, invariant);
            prover.Push();
            prover.Assert(gen.Not(f), true);
            EmptyErrorHandler h = new EmptyErrorHandler();
            prover.Check();
            var outcome = prover.CheckOutcomeCore(h);
            prover.Pop();

            Console.WriteLine("outcome is " + outcome);
            //if (forAllOPCheck(impl,pathConditions,invariant)) {
            //    return true;
            //}
            return false;
        }

        private static VCExpr forAllOPCheck ( Implementation impl,HashSet<VCExpr> pathConditions,VCExpr invariant ) {
            VCExpr formula = null;
            VCExpressionGenerator gen = new VCExpressionGenerator();
//            prover.Assert(VCExpressionGenerator.True, true);

            for (int i = 0; i < pathConditions.Count(); i++) {
                for (int j = i; j < pathConditions.Count(); j++) {
                    VCExpr p = getPairConstraint(impl,pathConditions.ElementAt(i),pathConditions.ElementAt(j));
                    List<VCExpr> argsToMyFunction = new List<VCExpr>();
                    VCExpr[] lis = new VCExpr[1];
                    lis[0] = gen.Integer(BigNum.ONE);                    
                    //prover.Assert(gen.Not(p),true);
                    //EmptyErrorHandler h = new EmptyErrorHandler();
                    //prover.Check();
                    //var outcome = prover.CheckOutcomeCore(h);
                    //prover.Pop();
                    //Console.WriteLine("outcome is : " + outcome);
                    if (formula == null) {
                        formula = p;
                    } else {
                        formula = gen.Or(formula,p);
                    }
                }
            }
            return formula;
        }

        /// <summary>
        /// returns a formula that if satisfiable implies the method is not op
        /// </summary>
        /// <param name="impl"></param>
        /// <param name="expr1"></param>
        /// <param name="expr2"></param>
        /// <returns></returns>
        public static VCExpr getPairConstraint ( Implementation impl,VCExpr expr1,VCExpr expr2 ) {
            //Console.WriteLine("Pc1 : " + expr1 + "\n\n");
            //Console.WriteLine("Pc2 : " + expr2 + "\n\n");
            VCExpressionGenerator gen = new VCExpressionGenerator();
            VCExpr ret = null;
            VCExprSubstitution subst = new VCExprSubstitution();
            SubstitutingVCExprVisitor substituter = new SubstitutingVCExprVisitor(gen);           
            HashSet<VCExprVar> var1 = getVariables(expr1);
            HashSet<VCExprVar> var2 = getVariables(expr2);
            //find the return variable
            String retVarName = impl.Proc.Name + "_" + impl.Proc.OutParams[0].Name;           
            VCExprVar retVar2 = null;
            VCExprVar retVar1 = null;
            HashSet<VCExprVar> inParamsVCExpr = new HashSet<VCExprVar>();
            foreach (var variable in var2) {            // initilize retVar1,2; inParmamsVCExpr, subst
                VCExprVar t = gen.Variable("p2_" + variable.Name,variable.Type);
                subst[variable] = t;
                if (variable.Name.Contains(retVarName)) {
                    retVar2 = t;
                    retVar1 = variable;
                }
                foreach (var a in impl.Proc.InParams) {
                    if (a.Name.Equals(variable.Name))
                        inParamsVCExpr.Add(variable);
                }
            }
            VCExpr an = substituter.Mutate(expr2,subst);
            //Console.WriteLine(an);
            VCExpr pc = gen.And(an,expr1);
            VCExpr retEqual = gen.Eq(retVar2,retVar1);  // equate the return variables
            VCExpr retNotEqual = gen.Not(gen.Eq(retVar2,retVar1));  // unequal return variables
            VCExprVar z = null;
            VCExpr equalParams = null;
            foreach (var v in impl.Proc.InParams) {     // equate all the parameters           
                foreach (var n in inParamsVCExpr) {
                    if (n.Name.Equals(v.Name))
                        z = n;
                }
                VCExpr p = gen.Eq(z,subst[z]);
                if (equalParams == null) {
                    equalParams = p;
                } else {
                    equalParams = gen.And(equalParams,p);
                }
            }
            //ret = gen.Not(gen.Implies(gen.And(pc,equalParams),retEqual));
            //ret = gen.Implies(gen.And(pc, equalParams), retEqual);
            ret = gen.And(gen.And(pc,equalParams),retNotEqual);
            List<VCExprVar> varList = new List<VCExprVar>();
            foreach (var v in var1) {
                varList.Add(v);
            }
            foreach (var v in var2) {
                varList.Add(subst[v] as VCExprVar);
            }            
            List<VCExpr> k = new List<VCExpr>();
            k.Add(ret); 
            VCTrigger s = new VCTrigger(true,k);
            List<VCTrigger> trig = new List<VCTrigger>();
            //ret = gen.Forall(varList,trig,ret);
            ret = gen.Exists(varList,trig,ret);
            //Console.WriteLine("\n" + ret);    
            //ret = gen.Not(ret);
            return ret; 
        }

        private static HashSet<VCExprVar> getVariables ( VCExpr expr1 ) {
            HashSet<VCExprVar> ret = new HashSet<VCExprVar>();
            getVariables(expr1,ret);
            return ret; 
        }

        private static void getVariables ( VCExpr expr,HashSet<VCExprVar> ret ) {
            if (expr is VCExprConstant || expr is VCExprContracts || expr is VCExprOp || expr is VCExprLiteral)
                return;
            if (expr is VCExprLet)
                return;
            if (expr is VCExprNAry) {
                VCExprNAry nExp = expr as VCExprNAry;
                for (int ctr = 0; ctr < nExp.Arity; ctr++) {
                    VCExpr arg = nExp[ctr];
                    VCExprVar argVar = arg as VCExprVar;
                    if (argVar != null) {
                        ret.Add(argVar);
                    } else {
                        getVariables(arg,ret);
                    }
                }
            }
        }

        private static void setupUninterpretedFunction ( Implementation impl,Program prog ) {
            //Console.WriteLine(impl.);

            //Console.WriteLine("hello   ((((((((((((((((((( hello");

            Function functionfromcode = null;
            NAryExpr naryexpr = null;
            foreach (Block b in impl.Blocks) {
                if (functionfromcode != null) break;
                foreach (var c in b.cmds) {
                    AssumeCmd acmd = c as AssumeCmd;
                    if (acmd != null) {
                        naryexpr = acmd.Expr as NAryExpr;
                        if (naryexpr != null) {
                            FunctionCall call = naryexpr.Fun as FunctionCall;
                            if (call != null) {
                                functionfromcode = call.Func;
                            }
                        }
                    }
                }
            }
            List<Variable> newinparams = new List<Variable>();
            //console.writeline("args size : " + naryexpr.args.count());                            
            //list<expr> newargs = new list<expr>(naryexpr.args);
            newinparams.AddRange(functionfromcode.InParams);
            //for (int i = 0; i < functionfromcode.InParams.Count; i++) {
            //    Console.WriteLine(functionfromcode.InParams.ElementAt(i).ToString());
            //}
            
            for (int i = 0; i < prog.GlobalVariables.Count; i++) {
                newinparams.RemoveRange(0, 1);
                newinparams.RemoveRange(newinparams.Count() - 1, 1);
            }
            Variable retvar = newinparams.ElementAt(newinparams.Count() - 1);
            newinparams.RemoveRange(newinparams.Count() - 1, 1);
            myFunction = new Function(Token.NoToken, "my_" + functionfromcode.Name, newinparams, retvar);
            //Console.WriteLine("globals :" + prog.GlobalVariables.Count());
            //Console.WriteLine("params : " + functionfromcode.InParams.Count());
            //Console.WriteLine("new length :" + newinparams.Count);
            //Console.WriteLine(myFunction);
            //for(int i = 0; i < myFunction.InParams.Count; i++) {
            //    Console.WriteLine(myFunction.InParams.ElementAt(i).UniqueId);
            //}
            //Environment.Exit(0);
            //myFunction = prog.FindFunction("foo");
           
            //Console.WriteLine(prog.Functions);
            //Console.WriteLine(prog.FindFunction("my_Fact"));
        }
        private static VCExpr forSomeOPCheck ( Implementation impl,HashSet<VCExpr> paths,VCExpr invariant ) {

            VCExpressionGenerator gen = new VCExpressionGenerator();
            for (int i = 0; i < paths.Count(); i++) {
                var ee = equateToFunction(impl,paths.ElementAt(i));
            }
            return null;
        }

        private static VCExpr equateToFunction (Implementation impl, VCExpr vcExpr ) {
            VCExpr retVar;

            Console.WriteLine(vcExpr);
            return null;
        }
        private static VCExpr getInvariant ( Implementation impl ) {
            // to be modified to read the invariant in text.
            VCExpr ret = null;
            var translator = prover.Context.BoogieExprTranslator;
            BasicType b = new BasicType(SimpleType.Int);
            VCExpressionGenerator gen = new VCExpressionGenerator();
            VCExprVar g = gen.Variable("nineteen",new BasicType(SimpleType.Int));
            g = (VCExprVar)translator.Translate(impl.Proc.Modifies.ElementAt(0));

            VCExpr m1 = gen.Integer(BigNum.FromInt(-1));
            ret = gen.Eq(g,m1);

            Expr eighteen = new LiteralExpr(Token.NoToken,BigNum.FromInt(18));
            Expr nineteen = new LiteralExpr(Token.NoToken,BigNum.FromInt(19));
            List<Expr> arg = new List<Expr>();
            arg.Add(eighteen);
            FunctionCall myCall = new FunctionCall(myFunction);
            Expr call = new NAryExpr(Token.NoToken,myCall,arg);
            call.Resolve(new ResolutionContext(null));
            call.Typecheck(new TypecheckingContext(null));
            List<VCExpr> arg1 = new List<VCExpr>();
            arg1.Add(translator.Translate(call));
            arg1.Add(gen.Integer(BigNum.FromInt(19)));
            VCExpr ret2 = gen.Function(VCExpressionGenerator.MulIOp,arg1);
            ret2 = gen.Eq(g,ret2);

            ret = gen.Or(ret,ret2);
            return ret;
        }
    }
}